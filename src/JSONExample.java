import models.Student;
//import com.fasterxml.jackson.databind.ObjectMapper;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;

public class JSONExample {

    public static String customerToJSON(Student student) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(student);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return s;
    }

    public static Student JSONToStudent(String s) {

        ObjectMapper mapper = new ObjectMapper();
        Student student = null;

        try {
            student = mapper.readValue(s, Student.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return student;
    }


    public static void main(String[] args) {
        Student student = new Student();
        Student student2 = new Student();
        Student student3 = null;

        ArrayList<String> courses = new ArrayList<>();

        courses.add("CIT360");
        courses.add("CIT325");
        courses.add("FDSCI101");

        student.setName("Daniel");
        student.setAge(25);
        student.setPhone("9 9988-8656");
        student.setCourses(courses);


        System.out.println("========= First Student ========");
        String json = JSONExample.customerToJSON(student);
        System.out.println(json);


        Student studentJson = JSONExample.JSONToStudent(json);
        System.out.println(studentJson);


        System.out.println("========= Second Student ========");
        String json2 = JSONExample.customerToJSON(student2);
        System.out.println(json2);


        Student studentJson2 = JSONExample.JSONToStudent(json2);
        System.out.println(studentJson2);

        //Prints null because the attribute is null
        System.out.println(studentJson2.getName());


        System.out.println("========= Third Student ========");
        String json3 = JSONExample.customerToJSON(student3);
        System.out.println(json3);


        Student studentJson3 = JSONExample.JSONToStudent(json3);
        System.out.println(studentJson3);

        try{
            //Throws an exception because the object is null and has no getName() method
            System.out.println(studentJson3.getName());
        }catch (NullPointerException ex){
            System.out.println(ex.getMessage());
        }



    }
}
